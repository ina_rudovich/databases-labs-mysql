use lab3;


#1.1--------------------------------------------
SELECT 
    *
FROM
    prepods;
    
#1.2--------------------------------------------

SELECT 
    last_name, first_name
FROM
    students
WHERE
    group_id = 1
ORDER BY last_name;

#1.3--------------------------------------------

SELECT 
    *
FROM
    students
WHERE
    last_name LIKE 'O%';
    
#2.1-------------------------------------------
SELECT 
    val
FROM
    rating
WHERE
    YEAR(`date`) = '2016'
        AND MONTH(`date`) = '10'
        AND is_absent = 'N'
        AND prepods_id = (SELECT 
            id
        FROM
            prepods
        WHERE
            last_name = 'Provolocki');
#2.2-----------------------------------------

SELECT 
    student_id, last_name, first_name, group_id, `date`
FROM
    rating
        JOIN
    students ON (student_id = students.id)
WHERE
    is_absent = 'Y';

#2.3----------------------------------------

SELECT 
    rating.val,
    rating.`date`,
    students.last_name,
    students.first_name,
    prepods.last_name,
    prepods.first_name,
    faculty.`name`,
    groups.`name`
FROM
    rating
        JOIN
    prepods ON rating.prepods_id = prepods.id
        JOIN
    students ON rating.student_id = students.id
        JOIN
    groups ON students.group_id = groups.id
        JOIN
    faculty ON groups.faculty_id = faculty.id;

#2.4----------------------------------------

SELECT DISTINCT
    rating.`date`,
    subjects.`name`,
    type_study_load.`name`,
    prepods.last_name,
    prepods.first_name
FROM
    rating
        JOIN
    students ON student_id = students.id
        JOIN
    prepods ON rating.prepods_id = prepods.id
        JOIN
    lessons ON (students.group_id = lessons.group_id
        AND rating.study_id = lessons.study_id)
        JOIN
    study_load ON rating.study_id = study_load.id
        JOIN
    type_study_load ON study_load.type_study_id = type_study_load.id
        JOIN
    subjects ON study_load.subject_id = subjects.id 
WHERE rating.prepods_id != lessons.prepod_id;

#2.5---------------------------------------------------

SELECT DISTINCT
    prepods.last_name,
    prepods.first_name,
    groups.`name`
FROM
    rating
        JOIN
    prepods
        JOIN
    students ON student_id = students.id
        JOIN
    groups ON group_id = groups.id
ORDER BY prepods.last_name;

#2.6--------------------------------------------------
SELECT DISTINCT
    prepods.last_name,
    prepods.first_name,
    groups.`name`
FROM
    rating
        RIGHT JOIN
    prepods ON prepods_id = prepods.id
    
        left JOIN
    students ON student_id = students.id
        left JOIN
    groups ON group_id = groups.id
	ORDER BY prepods.last_name;

#2.7--------------------------------------------------

select
	faculty.id,
    faculty.`name`
from
	faculty
		left join 
	groups ON groups.faculty_id = faculty.id
where groups.id is null;
