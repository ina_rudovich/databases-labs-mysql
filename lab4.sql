use lab3;
SET SQL_SAFE_UPDATES = 0;


#--------------------------------------------------
insert into groups(`name`) values
	('123456');
#--------------------------------------------------
insert into students(last_name) values
	('Ivanov'),
    ('Petrov');

UPDATE students 
SET 
    group_id = (SELECT 
            id
        FROM
            groups
        WHERE
            `name` = '123456');
UPDATE groups 
SET 
    head_group = (SELECT 
            id
        FROM
            students
        WHERE
            last_name = 'Ivanov')
WHERE
    `name` = '123456';

#-------------------------------------------------
insert into faculty(`name`) values
	('FCSN'),
	('FITC');

#-------------------------------------------------

insert into prepods (last_name, faculty_id) values
	('Vasechkin', (select id from faculty where `name`='FCSN')),
    ('Petechkin', (select id from faculty where `name`='FITC')); 

#-------------------------------------------------
#?????????????????????????????????????????????????
insert into type_study_load (`name`) value
	('Lection');
insert into subjects (`name`) value
	('Introduction to the specialty');
#?????????????????????????????????????????????????добавляется только первый раз после построения базы
insert into study_load (subject_id, type_study_id, id) value(
	(select id from subjects where `name`='Introduction to the specialty'),
    (select id from type_study_load where `name`='Lection'),
    'Introduction_l'
);
insert into lessons (group_id, prepod_id, study_id) values(
	(SELECT id from groups WHERE `name`='123456'),
    (SELECT id from prepods WHERE Last_name='Vasechkin'),
    ('Introduction_l')
);

#---------------------------------------------------

insert into rating (`Date`, val, student_id, study_id, prepods_id, is_absent)
values
(		'2017-10-17',
		 10, 
		(SELECT id from students WHERE last_name ='Ivanov'),
        'Introduction_l',
        (SELECT prepod_id from lessons WHERE study_id='Introduction_l'),
        'Y'
);
insert into rating (`Date`, val, student_id, study_id, prepods_id)
values
(		'2017-10-17',
		 NULL, 
		(SELECT id from students WHERE last_name ='Petrov'),
        'Introduction_l',
        (SELECT prepod_id from lessons WHERE study_id='Introduction_l')
);

insert into students (Last_name) values
	('Kozlov'),
    ('Popov'),
    ('Pavlov'),
    ('Potapov'),
    ('Fedorov');
 
insert into subjects (`name`) value ('PPO'), ('OOP'), ('MMA'), ('DB');


insert into study_load (subject_id, type_study_id, id)
values 
	((SELECT id from subjects where `name` = 'PPO'), (select id from type_study_load where `name`='Lection'), "PPO_st"),
    ((SELECT id from subjects where `name` = 'OOP'), (select id from type_study_load where `name`='Lection'), "OOP_st"),
    ((SELECT id from subjects where `name` = 'MMA'), (select id from type_study_load where `name`='Lection'), "MMA_st"),
    ((SELECT id from subjects where `name` = 'DB'),  (select id from type_study_load where `name`='Lection'), "DB_st");

call add_marks();

DELETE FROM lessons;
DELETE FROM rating;
DELETE FROM prepods;
DELETE FROM study_load;
DELETE FROM type_study_load;
DELETE FROM subjects;
UPDATE students 
SET 
	group_id = NULL;
DELETE FROM groups;
DELETE FROM students;
delete from faculty;

drop procedure add_marks;

DELIMITER //
CREATE PROCEDURE add_marks ()
  begin
	declare i int default (SELECT id from students WHERE Last_name ='Kozlov');
	declare j int default (SELECT id from students WHERE Last_name ='Fedorov');
	while i != j do
		insert into rating (`Date`, val, student_id, study_id, prepods_id, is_absent)
		values
			('2017-10-22', (SELECT RAND()*(11-1)+1), i, 'PPO_st', (SELECT id from prepods WHERE Last_Name='Vasechkin'),'y'),
			('2017-10-22', (SELECT RAND()*(11-1)+1), i, 'OOP_st', (SELECT id from prepods WHERE Last_Name='Vasechkin'), 'y'),
			('2017-10-22', (SELECT RAND()*(11-1)+1), i, 'MMA_st', (SELECT id from prepods WHERE Last_Name='Vasechkin'), 'y'),
			('2017-10-22', (SELECT RAND()*(11-1)+1), i, 'DB_st', (SELECT id from prepods WHERE Last_Name='Vasechkin'), 'y');
		set i = i + 1;
	end while;
  end
//
