use lab3;
delete from rating;
Insert into rating
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (10, str_TO_DATE('09,14,2016', '%m,%e,%Y'), 5, 6, 10, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (11, str_TO_DATE('09,14,2016', '%m,%e,%Y'), 5, 14, 6, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (13, str_TO_DATE('09,14,2016', '%m,%e,%Y'), 8, 20, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (17, str_TO_DATE('09,14,2016', '%m,%e,%Y'), 8, 25, 10, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (20, str_TO_DATE('21,5,2013','%d,%m,%Y'), 7, 8, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (21, str_TO_DATE('09,14,2016', '%m,%e,%Y'), 7, 16, 5, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (26, str_TO_DATE('09,14,2016', '%m,%e,%Y'), NULL, 20, 3, 
    8, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (27, str_TO_DATE('09,14,2016', '%m,%e,%Y'), 6, 14, 1, 
    1, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (35, str_TO_DATE('09,21,2016', '%m,%e,%Y'), 6, 25, 1, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (37, str_TO_DATE('09,21,2016', '%m,%e,%Y'), 6, 6, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (39, str_TO_DATE('09,21,2016', '%m,%e,%Y'), 10, 4, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (40, str_TO_DATE('09,21,2016', '%m,%e,%Y'), 7, 5, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (51, str_TO_DATE('09,28,2016', '%m,%e,%Y'), NULL, 8, 2, 
    3, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (53, str_TO_DATE('09,28,2016', '%m,%e,%Y'), 6, 16, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (54, str_TO_DATE('09,28,2016', '%m,%e,%Y'), 6, 12, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (58, str_TO_DATE('09,28,2016', '%m,%e,%Y'), 8, 18, 1, 
    1, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (60, str_TO_DATE('09,28,2016', '%m,%e,%Y'), 7, 8, 2, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (69, str_TO_DATE('09,28,2016', '%m,%e,%Y'), 10, 24, 5, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (70, str_TO_DATE('10,05,2016', '%m,%e,%Y'), 9, 17, 9, 
    10, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (72, str_TO_DATE('10,05,2016', '%m,%e,%Y'), 9, 13, 10, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (77, str_TO_DATE('10,05,2016', '%m,%e,%Y'), 8, 23, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (81, str_TO_DATE('10,05,2016', '%m,%e,%Y'), 5, 13, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (85, str_TO_DATE('10,05,2016', '%m,%e,%Y'), NULL, 13, 5, 
    4, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (91, str_TO_DATE('10,12,2016', '%m,%e,%Y'), 7, 10, 9, 
    6, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (93, str_TO_DATE('10,12,2016', '%m,%e,%Y'), NULL, 2, 8, 
    5, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (95, str_TO_DATE('10,12,2016', '%m,%e,%Y'), 8, 13, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (101, str_TO_DATE('10,12,2016', '%m,%e,%Y'), 7, 19, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (108, str_TO_DATE('10,12,2016', '%m,%e,%Y'), 9, 24, 6, 
    8, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (113, str_TO_DATE('10,19,2016', '%m,%e,%Y'), 5, 7, 5, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (114, str_TO_DATE('10,19,2016', '%m,%e,%Y'), 5, 11, 5, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (115, str_TO_DATE('10,19,2016', '%m,%e,%Y'), 9, 18, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (116, str_TO_DATE('10,19,2016', '%m,%e,%Y'), 9, 19, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (118, str_TO_DATE('10,19,2016', '%m,%e,%Y'), 9, 2, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (129, str_TO_DATE('10,19,2016', '%m,%e,%Y'), 8, 3, 3, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (142, str_TO_DATE('10,26,2016', '%m,%e,%Y'), NULL, 3, 7, 
    4, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (143, str_TO_DATE('10,26,2016', '%m,%e,%Y'), 8, 25, 2, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (158, str_TO_DATE('11,02,2016', '%m,%e,%Y'), NULL, 7, 5, 
    4, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (159, str_TO_DATE('11,02,2016', '%m,%e,%Y'), 6, 18, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (170, str_TO_DATE('11,09,2016', '%m,%e,%Y'), NULL, 24, 8, 
    5, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (178, str_TO_DATE('11,09,2016', '%m,%e,%Y'), 5, 9, 2, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (183, str_TO_DATE('11,09,2016', '%m,%e,%Y'), 7, 1, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (189, str_TO_DATE('11,09,2016', '%m,%e,%Y'), 8, 12, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (192, str_TO_DATE('11,16,2016', '%m,%e,%Y'), NULL, 7, 10, 
    8, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (194, str_TO_DATE('11,16,2016', '%m,%e,%Y'), 6, 6, 10, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (197, str_TO_DATE('11,16,2016', '%m,%e,%Y'), 6, 4, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (201, str_TO_DATE('11,16,2016', '%m,%e,%Y'), 5, 13, 6, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (215, str_TO_DATE('11,23,2016', '%m,%e,%Y'), 8, 14, 3, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (216, str_TO_DATE('11,23,2016', '%m,%e,%Y'), 5, 22, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (217, str_TO_DATE('11,23,2016', '%m,%e,%Y'), 8, 3, 10, 
    6, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (221, str_TO_DATE('11,23,2016', '%m,%e,%Y'), 9, 21, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (222, str_TO_DATE('11,23,2016', '%m,%e,%Y'), 7, 13, 9, 
    6, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (225, str_TO_DATE('11,23,2016', '%m,%e,%Y'), 6, 1, 9, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (231, str_TO_DATE('11,30,2016', '%m,%e,%Y'), 10, 4, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (233, str_TO_DATE('11,30,2016', '%m,%e,%Y'), 5, 25, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (238, str_TO_DATE('11,30,2016', '%m,%e,%Y'), 7, 22, 2, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (244, str_TO_DATE('11,30,2016', '%m,%e,%Y'), 4, 6, 9, 
    6, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (245, str_TO_DATE('11,30,2016', '%m,%e,%Y'), 4, 5, 4, 
    1, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (246, str_TO_DATE('11,30,2016', '%m,%e,%Y'), NULL, 5, 7, 
    4, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (247, str_TO_DATE('11,30,2016', '%m,%e,%Y'), 7, 5, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (253, str_TO_DATE('12,07,2016', '%m,%e,%Y'), 9, 16, 3, 
    8, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (259, str_TO_DATE('12,07,2016', '%m,%e,%Y'), 9, 6, 2, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (261, str_TO_DATE('12,07,2016', '%m,%e,%Y'), 8, 18, 9, 
    6, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (263, str_TO_DATE('12,07,2016', '%m,%e,%Y'), 8, 14, 6, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (265, str_TO_DATE('12,07,2016', '%m,%e,%Y'), 8, 14, 6, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (268, str_TO_DATE('12,07,2016', '%m,%e,%Y'), 7, 23, 2, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (270, str_TO_DATE('12,14,2016', '%m,%e,%Y'), 4, 9, 10, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (277, str_TO_DATE('12,14,2016', '%m,%e,%Y'), 5, 16, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (278, str_TO_DATE('12,14,2016', '%m,%e,%Y'), 4, 22, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (279, str_TO_DATE('12,14,2016', '%m,%e,%Y'), 5, 17, 9, 
    6, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (290, str_TO_DATE('12,21,2016', '%m,%e,%Y'), 6, 21, 3, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (291, str_TO_DATE('12,21,2016', '%m,%e,%Y'), 9, 22, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (295, str_TO_DATE('12,21,2016', '%m,%e,%Y'), 4, 19, 5, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (297, str_TO_DATE('12,21,2016', '%m,%e,%Y'), 5, 5, 5, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (298, str_TO_DATE('12,21,2016', '%m,%e,%Y'), 8, 20, 2, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (306, str_TO_DATE('12,21,2016', '%m,%e,%Y'), 5, 6, 10, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (311, str_TO_DATE('12,28,2016', '%m,%e,%Y'), 4, 11, 2, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (318, str_TO_DATE('12,28,2016', '%m,%e,%Y'), 9, 18, 6, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (321, str_TO_DATE('12,28,2016', '%m,%e,%Y'), 10, 10, 2, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (326, str_TO_DATE('12,28,2016', '%m,%e,%Y'), 7, 14, 7, 
    4, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (334, str_TO_DATE('01,04,2017', '%m,%e,%Y'), NULL, 18, 6, 
    3, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (335, str_TO_DATE('01,04,2017', '%m,%e,%Y'), 5, 1, 2, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (337, str_TO_DATE('01,04,2017', '%m,%e,%Y'), 7, 13, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (340, str_TO_DATE('01,04,2017', '%m,%e,%Y'), 5, 19, 3, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (343, str_TO_DATE('01,04,2017', '%m,%e,%Y'), 6, 15, 2, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (345, str_TO_DATE('01,04,2017', '%m,%e,%Y'), 7, 9, 8, 
    5, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (352, str_TO_DATE('01,11,2017', '%m,%e,%Y'), 5, 15, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (355, str_TO_DATE('01,11,2017', '%m,%e,%Y'), 4, 19, 6, 
    2, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (356, str_TO_DATE('01,11,2017', '%m,%e,%Y'), 6, 3, 3, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (358, str_TO_DATE('01,11,2017', '%m,%e,%Y'), 8, 19, 3, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (359, str_TO_DATE('01,11,2017', '%m,%e,%Y'), 9, 18, 3, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (374, str_TO_DATE('01,18,2017', '%m,%e,%Y'), 5, 22, 4, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (375, str_TO_DATE('01,18,2017', '%m,%e,%Y'), NULL, 12, 5, 
    4, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (377, str_TO_DATE('01,18,2017', '%m,%e,%Y'), NULL, 14, 10, 
    7, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (378, str_TO_DATE('01,18,2017', '%m,%e,%Y'), 5, 6, 10, 
    7, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (379, str_TO_DATE('01,18,2017', '%m,%e,%Y'), 4, 13, 9, 
    6, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (380, str_TO_DATE('01,18,2017', '%m,%e,%Y'), NULL, 4, 3, 
    2, 'Y');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (389, str_TO_DATE('01,18,2017', '%m,%e,%Y'), 7, 9, 3, 
    3, 'N');
Insert into RATING
   (ID, DATE, VAL, STUDENT_ID, STUDY_ID, 
    PREPODS_ID, IS_ABSENT)
 Values
   (392, str_TO_DATE('01,25,2017', '%m,%e,%Y'), 5, 22, 9, 
    4, 'N');
COMMIT;
