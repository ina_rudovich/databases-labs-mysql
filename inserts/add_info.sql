use lab3;


DELETE FROM lessons;
DELETE FROM rating;
DELETE FROM prepods;
DELETE FROM study_load;
DELETE FROM type_study_load;
DELETE FROM subjects;
UPDATE students 
SET 
	group_id = NULL;
DELETE FROM groups;
DELETE FROM students;
delete from faculty;


Insert into FACULTY
   (ID, NAME)
 Values
   (1, 'FCIS');
Insert into FACULTY
   (ID, NAME)
 Values
   (2, 'FITU');
Insert into FACULTY
   (ID, NAME)
 Values
   (3, 'IEF');
Insert into FACULTY
   (ID, NAME)
 Values
   (4, 'VF');
COMMIT;




Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (1, 'Sirotko', 'S.I.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (2, 'Stepankov', 'D.S.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (3, 'Provolocki', 'V.E.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (4, 'Erominek', 'K.R.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (5, 'Pashuk', 'A.V.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (6, 'Galkovski', 'A.V.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (7, 'Anisimov', 'V.Y.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (8, 'Stepankov', 'D.S.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (10, 'Surkov', 'D.A.', 1);
Insert into PREPODS
   (ID, LAST_NAME, FIRST_NAME, FACULTY_ID)
 Values
   (11, 'Surkov', 'K.A.', 1);
COMMIT;








Insert into GROUPS
   (ID, NAME, HEAD_GROUP, FACULTY_ID)
 Values
   (1, '453501', NULL, 1);
Insert into GROUPS
   (ID, NAME, HEAD_GROUP, FACULTY_ID)
 Values
   (2, '453502', NULL, 1);
Insert into GROUPS
   (ID, NAME, HEAD_GROUP, FACULTY_ID)
 Values
   (3, '453503', NULL, 1);
Insert into GROUPS
   (ID, NAME, HEAD_GROUP, FACULTY_ID)
 Values
   (4, '451001', NULL, 1);
Insert into GROUPS
   (ID, NAME, HEAD_GROUP, FACULTY_ID)
 Values
   (5, '451002', NULL, 1);
Insert into GROUPS
   (ID, NAME, HEAD_GROUP, FACULTY_ID)
 Values
   (6, '420601', NULL, 2);
Insert into GROUPS
   (ID, NAME, HEAD_GROUP, FACULTY_ID)
 Values
   (7, '420602', NULL, 2);
COMMIT;







Insert into TYPE_STUDY_LOAD
   (ID, NAME)
 Values
   (1, 'lection');
Insert into TYPE_STUDY_LOAD
   (ID, NAME)
 Values
   (2, 'practical');
Insert into TYPE_STUDY_LOAD
   (ID, NAME)
 Values
   (3, 'lab');
COMMIT;





Insert into SUBJECTS
   (ID, NAME, FACULTY_ID)
 Values
   (1, 'SP', 1);
Insert into SUBJECTS
   (ID, NAME, FACULTY_ID)
 Values
   (2, 'BD', 1);
Insert into SUBJECTS
   (ID, NAME, FACULTY_ID)
 Values
   (3, 'MNA', 1);
Insert into SUBJECTS
   (ID, NAME, FACULTY_ID)
 Values
   (4, 'PPO', 1);
Insert into SUBJECTS
   (ID, NAME, FACULTY_ID)
 Values
   (5, 'ABC', 1);
Insert into SUBJECTS
   (ID, NAME, FACULTY_ID)
 Values
   (6, 'IGI', 1);
Insert into SUBJECTS
   (ID, NAME, FACULTY_ID)
 Values
   (7, 'OSiSP', 1);
Insert into SUBJECTS
   (ID, NAME, FACULTY_ID)
 Values
   (8, 'SPP', 1);
COMMIT;




Insert into study_load
   (hours, subject_id, type_study_id, id)
 Values
   (96, 1, 1, '1                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (120, 1, 3, '2                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (128, 2, 1, '3                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (134, 2, 3, '4                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (144, 3, 3, '5                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (116, 3, 1, '6                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (78, 4, 3, '7                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (66, 4, 1, '8                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (114, 5, 1, '9                   ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (122, 5, 3, '10                  ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (66, 6, 1, '11                  ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (114, 6, 3, '12                  ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (120, 7, 1, '13                  ');
Insert into STUDY_LOAD
   (HOURS, SUBJECT_ID, TYPE_STUDY_ID, ID)
 Values
   (120, 7, 2, '14                  ');
COMMIT;






Insert into lessons
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 1, '1                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 2, '2                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 3, '3                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 3, '4                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 4, '5                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 3, '6                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 4, '7                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 5, '8                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 6, '9                   ');
Insert into LESSONS
   (GROUP_ID, PREPOD_ID, STUDY_ID)
 Values
   (1, 7, '10                  ');
COMMIT;


Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (12, 'Lyschitskaya', 'Larisa', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (13, 'Struck', 'Olga', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (14, 'Bozhko', 'Elena', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (15, 'Nemytko', 'Irina', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (16, 'Lvov', 'Irina', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (17, 'Kramnik', 'N. Ina', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (18, 'Kozlov', 'Galina Yanushevna', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (19, 'Malevankin', 'Irina', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (20, 'Kalenyuk', 'Marina O.', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (21, 'Krasnyachenko', 'Natalia', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (22, 'Shpak', 'Hope Yaroslavovna', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (23, 'Burakovskiy', 'Andrey', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (24, 'Ox', 'Pavel Aleksandrovich', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (25, 'Toisteva', 'Veronica V.', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (26, 'Sorokin', 'Elena A.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (27, 'Zheglanov', 'Irina E.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (28, 'Meleshkin', 'Denis', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (29, 'Lipstova', 'V. Alla', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (30, 'Krivodubsky', 'Natalia', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (31, 'Plavsky', 'Ludmila', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (32, 'Wisniewski', 'Vyacheslav V.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (33, 'Ksenzhik', 'Margaret L.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (34, 'Shimkovich', 'Olga', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (35, 'Kolbasova', 'Tatiana', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (36, 'Tatiana', 'Kovenya', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (37, 'Sergei', 'Glebko', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (38, 'Smirnov', 'Inessa M.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (39, 'Ignatovich', 'Olga', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (40, 'Tamara', 'Black', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (41, 'Bykov', 'Natalia V.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (42, 'Galina', 'Voronkov', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (43, 'Yuri', 'Karanets', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (44, 'Horushevskaya', 'Tatiana V.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (45, 'Katyushina', 'Lilia', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (46, 'Pinyuta', 'Julia Y.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (47, 'Vashkevich', 'Elena V.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (48, 'Markovich', 'Oksana', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (49, 'Sandrygaylo', 'Svetlana A.', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (50, 'Bogatko', 'Stanislaus Leonovna', 2);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (51, 'Maruschak', 'Elena Y.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (52, 'Tsikoto', 'Elena', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (53, 'Mulino', 'Lydia K.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (54, 'Michael', 'Galina', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (55, 'Salmanovich', 'Natalia M.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (56, 'Lazdovskaya', 'Tatiana Stanislavovna', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (57, 'Gorbunova', 'Raisa', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (58, 'Cherry', 'Tamara', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (59, 'Mikulicz', 'Elena', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (60, 'Belyaev', 'Valentina', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (61, 'Chupris', 'Irina', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (62, 'Korczak', 'Anna M.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (63, 'Oparin', 'Svetlana D. ', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (64, 'Kuksyuk', 'Lyudmila L.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (65, 'Skobkarev', 'Vitali V.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (66, 'Lisak', 'Olga', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (67, 'Krupich ',' Svetlana ', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (68, 'Platonov', 'Maria S.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (69, 'Kovzel', 'Lyudmila', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (70, 'Tropez', 'Aleksandr Aleksandrovich', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (71, 'Butevich', 'Julia Igorevna', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (72, 'Lyagush ',' Svetlana ', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (73, 'Gotsman', 'Galina', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (74, 'Prakopenko', 'Ales A.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (75, 'Kuzmitskaya', 'Olga O.', 3);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (76, 'Kozlovskaya', 'Lyudmila Yatskovna', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (77, 'Dechen', 'Elena Stanislavovna', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (78, 'Tamara', 'Kovaliova', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (79, 'Julia', 'Pikul', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (80, 'Vladimir', 'Baranov', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (81, 'Elena', 'Nemankova', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (82, 'Bukhanova', 'Svetlana E. ', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (83, 'Chaus', 'Tatiana', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (84, 'Ivanov ',' Svetlana ', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (85, 'Sinkevich', 'Elena D.', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (86, 'Novikova', 'Lyudmila Vitalievna', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (1, 'Shiloh', 'Galina', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (2, 'Daneiko', 'Larisa', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (3, 'Tihonenko', 'Natalia O.', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (4, 'Sytsevich', 'Natalia K.', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (5, 'Zhukovskaja', 'Elena', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (6, 'Yarmolovich', 'Tatiana L.', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (7, 'Antonov', 'Larisa G.', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (8, 'Shakhov', 'Vladimir Vladimirovich', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (9, 'Shulyakovskaya', 'Maria Igorevna', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (10, 'Lapko', 'Alexander Arkadevna', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (11, 'Nichiporuk ','Svetlana ', 1);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (87, 'White', 'Julia', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (88, 'Akulich', 'Anna V.', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (89, 'Popov', 'Olga', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (90, 'Kuntcevich', 'Elena I.', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (91, 'Olga', 'Sofronova', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (92, 'Svetlana', 'Sologub', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (93, 'Rita', 'Truhan', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (94, 'Love', 'Samantsova', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (95, 'Kutsenkova', 'Elena', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (96, 'Filipovich', 'Elena', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (97, 'Yukovich', 'Lyudmila G.', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (98, 'Vrublevskaya', 'Elena', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (99, 'Gavrosh', 'Ludmila', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (100, 'Gubko', 'Yanina losifovna', 4);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (101, 'Dzemidzenka', 'Jeanne E.', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (102, 'Chaykovskaya', 'Ludmila', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (103, 'Kahnovich', 'Stanislaus Stanislavovna', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (104, 'Rapevets', 'Larisa', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (105, 'Motevich', 'Natalia L.', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (106, 'Primachok', 'Elena', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (107, 'Petrova', 'Alla Vitalievna', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (108, 'Salnikova', 'Galina Adolfovna', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (109, 'Magdeeva', 'Elena', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (110, 'Goncharov', 'Andrey Aleksandrovich', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (111, 'Khamenok', 'M. Taisa', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (112, 'Pilipenko', 'Lyudmila L.', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (113, 'Rojco', 'Maria Arkadevna', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (114, 'pug', 'Ekaterina', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (115, 'Mackiewicz', 'V. Julia', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (116, 'Podelinskaya', 'Irina I.', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (117, 'Klymets', 'Lyudmila', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (118, 'Govryulyuk', 'Svetlana Slavovna ', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (119, 'Kumpel', 'Svetlana', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (120, 'skid', 'Lidiya', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (121, 'Petrakova', 'G. Oles', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (122, 'Scripcenco', 'Lyudmila G.', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (123, 'Iwashko', 'Sergey E.', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (124, 'Miron', 'Yanina Vyacheslavovna', 5);
Insert into STUDENTS
   (ID, LAST_NAME, FIRST_NAME, GROUP_ID)
 Values
   (125, 'Pavlyuchik', 'Lyudmila', 5);
COMMIT;
