drop database if exists lab3;
create database lab3;
use lab3;

CREATE TABLE IF NOT EXISTS faculty (
    id int not null auto_increment,
    `name` VARCHAR(30),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS groups (
    id int not null auto_increment,
    `name` VARCHAR(10),
    head_group int,
    faculty_id int,
    constraint PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS prepods (
    id int not null auto_increment,
    last_name VARCHAR(120) NOT NULL,
    first_name VARCHAR(120),
    faculty_id int,
    PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS students (
    id int not null auto_increment,
    last_name VARCHAR(60) NOT NULL,
    first_name VARCHAR(30),
    group_id int,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS study_load (
    hours NUMERIC,
    subject_id INT,
    type_study_id INT,
    id CHAR(20) NOT NULL,
    PRIMARY KEY (id),
    CHECK (hours >= 3)
);

CREATE TABLE IF NOT EXISTS lessons (
    group_id int,
    prepod_id int,
    study_id CHAR(20) NOT NULL,
    PRIMARY KEY (group_id , prepod_id , study_id)
);

CREATE TABLE IF NOT EXISTS rating (
    id int not null auto_increment,
    `date` DATE NOT NULL,
    val NUMERIC,
    student_id int not null,
    study_id CHAR(20) NOT NULL,
    prepods_id int,
    is_absent CHAR(1) DEFAULT 'N',
    PRIMARY KEY (id , study_id),
    check(val > 3 and val <= 10)
);

CREATE TABLE IF NOT EXISTS subjects (
    id INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(120) NOT NULL,
    faculty_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS type_study_load (
    id int not null auto_increment,
    `name` VARCHAR(120) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

ALTER TABLE groups add constraint fk_head_of_group foreign key (head_group) references students(id);
   
alter table groups add constraint fk_faculty_group foreign key (faculty_id) references faculty(id) on update cascade;

alter table prepods add constraint fk_study_prepods foreign key (faculty_id) references faculty(id) on update cascade;
alter table students add constraint fk_group_of_students foreign key (group_id) references groups(id) on update cascade;

alter table study_load
add constraint fk_type_study foreign key (type_study_id) references type_study_load(id) on update cascade;
alter table study_load
add constraint fk_subject_study foreign key (subject_id) references subjects(id) on update cascade;


alter table lessons add constraint fk_group_lessons foreign key (group_id) references groups(id) on update cascade;
alter table lessons add constraint fk_prepods_lessons foreign key (prepod_id) references prepods(id) on update cascade;
alter table lessons add constraint relationship20 foreign key (study_id) references study_load(id) on update cascade;

alter table rating add constraint fk_student_rating foreign key (student_id) references students(id) on update cascade;
alter table rating add constraint fk_prepod_rating foreign key (prepods_id) references prepods(id) on update cascade;
alter table rating add constraint fk_study_lessons foreign key (study_id) references study_load(id) on update cascade;

alter table subjects add constraint fk_faculty_subjects foreign key (faculty_id) references faculty(id) on update cascade;
