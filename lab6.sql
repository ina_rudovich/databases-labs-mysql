use lab3;

#1.1--------------------------------------------
SELECT 
    COUNT(*)
FROM
    (SELECT DISTINCT
        study_id, prepods_id, `date`
    FROM
        rating) AS t;

#1.2--------------------------------------------

select
	avg(val) as average_value,
    students.last_name,
    students.first_name
    from
		rating
	join
		students on students.id = student_id
	group by student_id 
    order by average_value desc
    limit 10;
    
#1.3----------------------------------------------

SELECT 
    COUNT(*)
FROM
    (SELECT 
        subject_id
    FROM
        rating
    JOIN study_load ON subject_id = study_load.subject_id
    GROUP BY subject_id) AS t;


SELECT 
    COUNT(*)
FROM
    (SELECT 
        subject_id
    FROM
        rating
    JOIN study_load ON subject_id = study_load.subject_id
    JOIN type_study_load ON type_study_id = type_study_id
    GROUP BY type_study_id) AS t;
    
#1.4-------------------------------------------------------
    
SELECT 
    prepods.last_name,
    prepods.first_name,
    COUNT(val) AS count_marks
FROM
    prepods
        LEFT JOIN
    rating ON rating.prepods_id = prepods.id
GROUP BY prepods_id
ORDER BY count_marks;


#1.5---------------------------------------------------------

SELECT 
    last_name, first_name, count_marks
FROM
    (SELECT 
        students.last_name, students.first_name, COUNT(val) AS count_marks
    FROM
        rating
    RIGHT JOIN students ON rating.student_id = students.id
    GROUP BY student_id) AS t
WHERE
    t.count_marks = 0;

#1.6--------------------------------------------------------



   #with t1 as 
   select * from(SELECT 
        students.last_name,
            students.first_name,
            groups.`name`,
            AVG(val) AS average_mark
    FROM
        rating
    RIGHT JOIN students ON rating.student_id = students.id
    JOIN groups ON students.group_id = groups.id
    GROUP BY student_id) as t1
    where average_mark=(
    select max(average_mark) from (SELECT 
        students.last_name,
            students.first_name,
            groups.`name`,
            AVG(val) AS average_mark
    FROM
        rating
    RIGHT JOIN students ON rating.student_id = students.id
    JOIN groups ON students.group_id = groups.id
    GROUP BY student_id) as t1)
